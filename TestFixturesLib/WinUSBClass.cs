﻿using System;
using System.Linq;
using System.Threading;
using MadWizard.WinUSBNet;
using System.Windows.Forms;
using System.Management;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
/********************************************************************
* TestFixturesLib contains the functions to communicate with        *
* Test Fixtures Bench                                               *
********************************************************************/
namespace TestFixturesLib
{
    public class WinUSBClass
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        static extern bool GetVolumeInformation(string Volume, StringBuilder VolumeName, uint VolumeNameSize, out uint SerialNumber, out uint SerialNumberLength, out uint flags, StringBuilder fs, uint fs_size);

        #region Field
        const string IF_GUID = "a5dcbf10-6530-11d2-901f-00c04fb951ed";
        const string VID_PID = "VID_1FD9&PID_0111";
        const int HEADER_SIZE = 3;
        const int BOOT_NB_BYTE = 3;
        const int BOOT_STATE_BYTE = 4;
        const int IN_RESP_SIZE = 4;
        const int ALOG_RESP_SIZE = 7;
        const int OUT_RESP_SIZE = 3;
        const int VERSION_RESP_SIZE = 10;
        const int BOARD_NUMBER_RESP_SIZE = 4;
        const int LENGTH_CHAR = 2;
        const int LENGTH_VERSION = 7;

        USBDevice _uSBDevice;
        USBPipeCollection _uSBPipeCollection;
        USBPipe _uSBpipeIN;
        USBPipe _uSBpipeOUT;
        USBNotifier _uSBNotifier;

        public event EventHandler _uSBArrivalEvent; // Event to indicate Arrival device.
        public event EventHandler _uSBRemovalEvent; // Event to indicate Removal device.

        int _debuggerNb;

        int _vid;
        int _pid;
        string _vidPid;
        #endregion

        #region Enum
        public enum IN_Nb: int
        {
            /// <summary>
            /// IN0_1
            /// </summary>
            IN0,
            /// <summary>
            /// IN1_1
            /// </summary>
            IN1,
            /// <summary>
            /// IN2_1
            /// </summary>
            IN2,
            /// <summary>
            /// IN3_1
            /// </summary>
            IN3,
            /// <summary>
            /// IN4_1
            /// </summary>
            IN4,
            /// <summary>
            /// IN5_1
            /// </summary>
            IN5,
            /// <summary>
            /// IN6_1
            /// </summary>
            IN6,
            /// <summary>
            /// IN7_1
            /// </summary>
            IN7,
            /// <summary>
            /// IN0_2
            /// </summary>
            IN8,
            /// <summary>
            /// IN1_2
            /// </summary>
            IN9,
            /// <summary>
            /// IN2_2
            /// </summary>
            IN10,
            /// <summary>
            /// IN3_2
            /// </summary>
            IN11,
            /// <summary>
            /// IN4_2
            /// </summary>
            IN12,
            /// <summary>
            /// IN5_2
            /// </summary>
            IN13,
            /// <summary>
            /// IN6_2
            /// </summary>
            IN14,
            /// <summary>
            /// IN7_2
            /// </summary>
            IN15,
            /// <summary>
            /// IN0_3
            /// </summary>
            IN16,
            /// <summary>
            /// IN1_3
            /// </summary>
            IN17,
            /// <summary>
            /// IN2_3
            /// </summary>
            IN18,
            /// <summary>
            /// IN3_3
            /// </summary>
            IN19,
            /// <summary>
            /// IN4_3
            /// </summary>
            IN20,
            /// <summary>
            /// IN5_3
            /// </summary>
            IN21,
            /// <summary>
            /// IN6_3
            /// </summary>
            IN22,
            /// <summary>
            /// IN7_3
            /// </summary>
            IN23,
            /// <summary>
            /// IN0_4
            /// </summary>
            IN24,
            /// <summary>
            /// IN1_4
            /// </summary>
            IN25,
            /// <summary>
            /// IN2_4
            /// </summary>
            IN26,
            /// <summary>
            /// IN3_4
            /// </summary>
            IN27,
            /// <summary>
            /// IN4_4
            /// </summary>
            IN28,
            /// <summary>
            /// IN5_4
            /// </summary>
            IN29,
            /// <summary>
            /// IN6_4
            /// </summary>
            IN30,
            /// <summary>
            /// IN7_4
            /// </summary>
            IN31
        }

        /// <summary>
        /// OUTPUT Number
        /// </summary>
        public enum OUT_Nb : int
        {
            /// <summary>
            /// CMD0_1
            /// </summary>
            OUT0,
            /// <summary>
            /// CMD1_1
            /// </summary>
            OUT1,
            /// <summary>
            /// CMD2_1
            /// </summary>
            OUT2,
            /// <summary>
            /// CMD3_1
            /// </summary>
            OUT3,
            /// <summary>
            /// CMD4_1
            /// </summary>
            OUT4,
            /// <summary>
            /// CMD5_1
            /// </summary>
            OUT5,
            /// <summary>
            /// CMD0_2
            /// </summary>
            OUT6,
            /// <summary>
            /// CMD1_2
            /// </summary>
            OUT7,
            /// <summary>
            /// CMD2_2
            /// </summary>
            OUT8,
            /// <summary>
            /// CMD3_2
            /// </summary>
            OUT9,
            /// <summary>
            /// CMD4_2
            /// </summary>
            OUT10,
            /// <summary>
            /// CMD5_2
            /// </summary>
            OUT11,
            /// <summary>
            /// CMD0_3
            /// </summary>
            OUT12,
            /// <summary>
            /// CMD1_3
            /// </summary>
            OUT13,
            /// <summary>
            /// CMD2_3
            /// </summary>
            OUT14,
            /// <summary>
            /// CMD3_3
            /// </summary>
            OUT15,
            /// <summary>
            /// CMD4_3
            /// </summary>
            OUT16 = 16,
            /// <summary>
            /// CMD5_3
            /// </summary>
            OUT17 = 17,
            /// <summary>
            /// CMD0_4
            /// </summary>
            OUT18,
            /// <summary>
            /// CMD1_4
            /// </summary>
            OUT19,
            /// <summary>
            /// CMD2_4
            /// </summary>
            OUT20,
            /// <summary>
            /// CMD3_4
            /// </summary>
            OUT21,
            /// <summary>
            /// CMD4_4
            /// </summary>
            OUT22,
            /// <summary>
            /// CMD5_4
            /// </summary>
            OUT23
        }

        /// <summary>
        /// ANALOG Input Number
        /// </summary>
        public enum ALOG_Nb : int
        {
            /// <summary>
            /// ALOG0_1
            /// </summary>
            ALOG0,
            /// <summary>
            /// ALOG1_1
            /// </summary>
            ALOG1,
            /// <summary>
            /// ALOG2_1
            /// </summary>
            ALOG2,
            /// <summary>
            /// ALOG3_1
            /// </summary>
            ALOG3,
            /// <summary>
            /// ALOG0_2
            /// </summary>
            ALOG4,
            /// <summary>
            /// ALOG1_2
            /// </summary>
            ALOG5,
            /// <summary>
            /// ALOG2_2
            /// </summary>
            ALOG6,
            /// <summary>
            /// ALOG3_2
            /// </summary>
            ALOG7,
            /// <summary>
            /// ALOG0_3
            /// </summary>
            ALOG8,
            /// <summary>
            /// ALOG1_3
            /// </summary>
            ALOG9,
            /// <summary>
            /// ALOG2_3
            /// </summary>
            ALOG10,
            /// <summary>
            /// ALOG3_3
            /// </summary>
            ALOG11,
            /// <summary>
            /// ALOG0_4
            /// </summary>
            ALOG12,
            /// <summary>
            /// ALOG1_4
            /// </summary>
            ALOG13 = 13,
            /// <summary>
            /// ALOG2_4
            /// </summary>
            ALOG14,
            /// <summary>
            /// ALOG3_4
            /// </summary>
            ALOG15,
            /// <summary>
            /// NO ALOG
            /// </summary>
            NONE = 99
        }

        /// <summary>
        /// BOOT pin Number
        /// </summary>
        public enum BOOT_Nb : int
        {
            /// <summary>
            /// BOOT1_1
            /// </summary>
            BOOT0,
            /// <summary>
            /// BOOT2_1
            /// </summary>
            BOOT1,
            /// <summary>
            /// BOOT1_2
            /// </summary>
            BOOT2,
            /// <summary>
            /// BOOT2_2
            /// </summary>
            BOOT3,
            /// <summary>
            /// BOOT1_3
            /// </summary>
            BOOT4,
            /// <summary>
            /// BOOT2_3
            /// </summary>
            BOOT5,
            /// <summary>
            /// BOOT1_4
            /// </summary>
            BOOT6,
            /// <summary>
            /// BOOT2_4
            /// </summary>
            BOOT7
        }

        /// <summary>
        /// List of USB port used in this test bench.
        /// </summary>
        public enum USB_Nb : int
        {
            /// <summary>
            /// PWR_CTL1_1
            /// </summary>
            USB1 = 1,
            /// <summary>
            /// PWR_CTL2_1
            /// </summary>
            USB2,
            /// <summary>
            /// PWR_CTL1_2
            /// </summary>
            USB3,
            /// <summary>
            /// PWR_CTL2_2
            /// </summary>
            USB4
        }

        /// <summary>
        /// AcquMode fo conso
        /// </summary>
        public enum ACQU_MODE : int
        {
            /// <summary>
            /// Single acquisition.
            /// </summary>
            SINGLE,
            /// <summary>
            /// Burst acquisition.
            /// </summary>
            BURST
        }

        /// <summary>
        /// Logic level state
        /// </summary>
        public enum STATE: int
        {
            /// <summary>
            /// low state
            /// </summary>
            LOW = 0,
            /// <summary>
            /// High state
            /// </summary>
            HIGH = 1
        }

        /// <summary>
        /// Select open fain output
        /// </summary>
        public enum OUTPUT : int
        {
            OPEN_DRAIN = 0,
            PUSH_PULL = 1
        }

        #endregion

        #region Accesser
        /// <summary>
        /// Access USB Device
        /// </summary>
        public USBDevice _usbDevice { get { return _uSBDevice; } set { _uSBDevice = value; } }
        public bool _winUsbConnected { get; set; }
        public bool _arrival { get; set; }
        public bool _removal { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for WinUSB_Class
        /// </summary>
        public WinUSBClass()
        {
            _debuggerNb = 0b00;
            _arrival = false;
            _removal = false;
            //USBDevice USBDevice = new USBDevice("\\?\usb#vid_1fd9&pid_0111#abcd123456789#{a5dcbf10-6530-11d2-901f-00c04fb951ed}");                     
        }

        /// <summary>
        /// Constructor with contol parameter to uses USB event
        /// </summary>
        /// <param name="ctrl"></param>
        public WinUSBClass(Control ctrl)
        {
            _debuggerNb = 0b00;
            _uSBNotifier = new USBNotifier(ctrl, IF_GUID);
            _uSBNotifier.Arrival += new USBEventHandler(Arrival);
            _uSBNotifier.Removal += new USBEventHandler(Removal);

            _arrival = false;
            _removal = false;
        }
        #endregion

        #region Method
        /// <summary>
        /// Search any WinUSB device connected
        /// </summary>
        public bool InitWinUSBDevice(ref string devicePath)
        {
            bool res = false;
            USBDeviceInfo[] winbUSBlist = new USBDeviceInfo[20];

            winbUSBlist = USBDevice.GetDevices(IF_GUID);

            // Search device with VID PID
            foreach (USBDeviceInfo deviceInfo in winbUSBlist)
            {
                if (deviceInfo.VID == 0x1FD9 && deviceInfo.PID == 0x0111)
                {
                    // Get USB device path
                    devicePath = deviceInfo.DevicePath;

                    try
                    {
                        // Create New WinUSB device
                        if (_uSBDevice == null)
                            _uSBDevice = new USBDevice(deviceInfo);
                    }
                    catch (Exception)
                    {
                        _uSBDevice.Dispose();
                        _uSBDevice = null;
                        throw;
                    }

                    _uSBPipeCollection = _uSBDevice.Pipes;

                    // Init USB pipe IN and OUT
                    if (GetUsbPipe())
                        res = true;

                    break;
                }
            }

            // Update winUsbConnected flag
            _winUsbConnected = res;

            return res;
        }

        /// <summary>
        /// Unset USB device
        /// </summary>
        public void DeInitWinUSBDevice()
        {
            //Reset Device
            if(_usbDevice != null)
            {              
                _uSBDevice.Dispose();
                _uSBDevice = null;

                _uSBPipeCollection = null;
                _uSBpipeIN = null;
                _uSBpipeOUT = null;
            }

            if (_uSBNotifier != null)
            {
                _uSBNotifier.Dispose();
                _uSBNotifier = null;
            }
            
        }

        /// <summary>
        /// Check USB device presence with VID PID
        /// </summary>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public static bool CheckWinUSBDevice(int vid, int pid, int deviceNb = 1)
        {
            bool bres = false;
            int count = 0;
            USBDeviceInfo[] array = USBDevice.GetDevices("a5dcbf10-6530-11d2-901f-00c04fb951ed");

            foreach (USBDeviceInfo val in array)
            {
                if (val.VID == vid && val.PID == pid)
                {
                    count++;
                    if (count == deviceNb)
                    {
                        bres = true;
                        break;
                    }
                }
            }

            //_winUsbConnected = bres;
            return bres;
        }

        /// Check USB device presence with VID PID
        /// </summary>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public static bool CheckWinUSBDevice(int vid, int pid, ref int count, int deviceNb = 1)
        {
            bool bres = false;
            count = 0;
            USBDeviceInfo[] array = USBDevice.GetDevices("a5dcbf10-6530-11d2-901f-00c04fb951ed");

            foreach (USBDeviceInfo val in array)
            {
                if (val.VID == vid && val.PID == pid)
                {
                    count++;
                    if (count == deviceNb)
                    {
                        bres = true;
                        break;
                    }
                }
            }

            //_winUsbConnected = bres;
            return bres;
        }

        /// <summary>
        /// Retrieve device path for corresponding usb vid pid
        /// </summary>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <param name="devicePath"></param>
        /// <returns></returns>
        public static bool CheckWinUSBDevice(int vid, int pid, ref string devicePath)
        {
            bool result = false;
            USBDeviceInfo[] devices = USBDevice.GetDevices("a5dcbf10-6530-11d2-901f-00c04fb951ed");
            USBDeviceInfo[] array = devices;
            foreach (USBDeviceInfo uSBDeviceInfo in array)
            {
                if (uSBDeviceInfo.VID == vid && uSBDeviceInfo.PID == pid)
                {
                    devicePath = uSBDeviceInfo.DevicePath;
                    result = true;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Search Debugger Device.
        /// </summary>
        public bool GetDebuggerUSBDevice()
        {
            bool bres = false;
            int debuggerNb = 0b00;
            int debugCnt = 0;
            USBDeviceInfo[] winbUSBlist = new USBDeviceInfo[20];            

            winbUSBlist = USBDevice.GetDevices(IF_GUID);

            // Search device with VID PID
            foreach (USBDeviceInfo deviceInfo in winbUSBlist)
            {
                if (deviceInfo.VID == 0x1FC9 && deviceInfo.PID == 0x000C)
                {
                    // Get booted debugger Nb
                    debuggerNb |= (1 << debugCnt);

                    // Get 2nd booted debuggers
                    if (debuggerNb == 0b11)
                    {
                        bres = true;
                        break;
                    }

                    debugCnt++;
                }
            }

            return bres;
        }

        /// <summary>
        /// Search Debugger Device.
        /// </summary>
        public bool GetBootedDebuggerUSBDevice()
        {
            bool bres = false;
            int debugCnt = 0;
            USBDeviceInfo[] winbUSBlist = new USBDeviceInfo[20];

            winbUSBlist = USBDevice.GetDevices(IF_GUID);

            // Search device with VID PID
            foreach (USBDeviceInfo deviceInfo in winbUSBlist)
            {
                if (deviceInfo.VID == 0x1FC9 && deviceInfo.PID == 0x0090)
                {                  
                    // Get booted debugger Nb
                    _debuggerNb |= (1 << debugCnt);

                    // Get 2nd booted debuggers
                    if (_debuggerNb == 0b11)
                    {
                        bres = true;
                        break;
                    }                 

                    debugCnt++;
                }
            }

            return bres;
        }

        /// <summary>
        /// Search Debugger Device.
        /// </summary>
        public void GetBenchCOMPortUSBDevice(ref string COMPortA, ref string COMPortB)
        {
            var deviceTree = new DeviceTree();
            string hubNb;
            int serialConverterNb = 0;

            // Search usb HUB where TESTBENCH is connected on (Using Mainboard USB driver name)
            foreach (var elyctisDevice in deviceTree.DeviceNodes.Where(d => d.Description == "Elyctis Driver"))
            {
                if (serialConverterNb == 2)
                    break;

                // Get usb HUB number
                hubNb = elyctisDevice.LocationInfo.Substring(11);

                // Search device on current usb HUB
                foreach (var device in deviceTree.DeviceNodes.Where(d => d.LocationInfo.Contains(hubNb)))
                {
                    if (serialConverterNb == 2)
                        break;

                    // Search USB serial converter
                    if (device.Description == "USB Serial Converter")
                    {
                        // Search device on HUB port#1
                        if (device.LocationInfo.Contains("Port_#0001"))
                        {
                            COMPortA = device.Children[0].FriendlyName;
                            COMPortA = COMPortA.Replace("USB Serial Port (", "").Replace(")", "");
                            serialConverterNb++;
                        }
                        // Search device on HUB port#2
                        else if (device.LocationInfo.Contains("Port_#0002"))
                        {
                            COMPortB = device.Children[0].FriendlyName;
                            COMPortB = COMPortB.Replace("USB Serial Port (", "").Replace(")", "");
                            serialConverterNb++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Search DFU device
        /// </summary>
        /// <param name="dfuInterface"></param>
        public bool GetDfuUSBDevice(ref string dfuInterface)
        {
            bool bres = false;
            DeviceTree deviceTree = new DeviceTree();
            foreach (DeviceNode item in deviceTree.DeviceNodes.Where((DeviceNode d) => d.Description == "DFU"))
            {
                dfuInterface = item.ToString();
                bres = true;
                break;
            }

            return bres;
        }

        /// <summary>
        /// Get and set USB pipe
        /// </summary>
        /// <returns></returns>
        public bool GetUsbPipe()
        {
            USBPipePolicy policy = null;
            bool res = false;
            bool res1 = false;
            bool res2 = false;
            
            foreach (USBPipe pipe in _uSBDevice.Pipes)
            {
                pipe.Abort();
                policy = pipe.Policy;
                policy.PipeTransferTimeout = 1000;

                if (pipe.IsIn)
                {
                    policy.AllowPartialReads = true;
                    policy.IgnoreShortPackets = false;
                    //policy.AutoClearStall = false;
                    _uSBpipeIN = pipe;

                    pipe.Flush();
                    _uSBDevice.ControlIn(0xC1, 0, 0, 0);
                    Thread.Sleep(10);
                    pipe.Flush();

                    res1 = true;
                }
                if (pipe.IsOut)
                {
                    _uSBpipeOUT = pipe;
                    res2 = true;
                }
            }

            res = res1 & res2;
            return res;
        }

        /// <summary>
        /// Write a command to device.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dataSize"></param>
        public bool SendCommand(byte[] cmd, int dataSize, ref byte[] resp, int timeout = 5000, bool pulseMode = false)
        {
            bool bres = false;
            resp = new byte[10] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            byte[] test = new byte[20];
            byte[] header = { 0x00, 0x00, 0x00 };

            if (_winUsbConnected)
            {
                bres = true;
                try
                {
                    //_uSBpipeIN.Flush();
                    _uSBpipeOUT.Write(cmd, 0, cmd.Length);

                    // Add waiting delay if acquisition is on pulse.
                    if (pulseMode)
                        Thread.Sleep(250);
                    else
                        Thread.Sleep(30);

                    DateTime end = DateTime.Now.AddMilliseconds(timeout);
                    do
                    {
                        _uSBpipeIN.Read(resp, 0, dataSize);
                    }
                    while ((resp[0] != cmd[0]) && (DateTime.Now < end));

                    if (DateTime.Now >= end)
                        bres = false;

                    //_uSBpipeIN.Flush();
                }
                catch (Exception exc)
                {
                    bres = false;
                    //throw;
                }
            }

            return bres;
        }

        /// <summary>
        /// Send an IN command.
        /// </summary>
        public bool Send_IN_Command(int IN_nb, ref string response, ref int value)
        {
            byte[] cmd = { 0x49, 0x00, 0x01, (byte)IN_nb };
            byte[] IN_number = new byte[1] { 0xFF };
            bool bres = false;
            byte[] resp = new byte[IN_RESP_SIZE];

            // Default value;
            value = 8888;

            // Send IN command, wait only ACK but no response value.
            if (SendCommand(cmd, IN_RESP_SIZE, ref resp))
            {
                response = BitConverter.ToString(resp, 0, IN_RESP_SIZE);
                value = resp[IN_RESP_SIZE - 1];
                bres = true;
            }

            return bres;
        }

        /// <summary>
        /// Send an ALOG command.
        /// </summary>
        public bool Send_ALOG_Command(int alogNb, int acqMode, ref string response, ref int value)
        {
            byte[] cmd = new byte[5] { 0x41, 0x00, 0x02, (byte)alogNb, (byte)acqMode };
            bool bres = false;
            bool pulseMode = false; ;
            byte[] resp = new byte[ALOG_RESP_SIZE];

            // Default value;
            value = 8888;

            // Check id acquisition on rising edge to add USB waiting delay
            if (acqMode == 0x01)
                pulseMode = true;

            // Send ALOG command, Get response and value
            if (SendCommand(cmd, ALOG_RESP_SIZE, ref resp, 500, pulseMode))
            {
                response = BitConverter.ToString(resp, 0, ALOG_RESP_SIZE);
                value = (resp[ALOG_RESP_SIZE - 2] << 8) + resp[ALOG_RESP_SIZE - 1];
                bres = true;
            }

            return bres;
        }

        /// <summary>
        /// GET conso command.
        /// </summary>
        public bool Send_CONSO_Command(int usbSlot, ref string response, ref int value)
        {
            byte[] cmd = new byte[5] { 0x43, 0x00, 0x02, (byte)usbSlot, (int)ACQU_MODE.BURST };
            bool bres = false;
            bool pulseMode = true;
            byte[] resp = new byte[ALOG_RESP_SIZE];

            // Default value;
            value = 8888;

            // Send CONSO command, Get response and value
            if (SendCommand(cmd, ALOG_RESP_SIZE, ref resp, 500, pulseMode))
            {
                response = BitConverter.ToString(resp, 0, ALOG_RESP_SIZE);
                value = (resp[ALOG_RESP_SIZE - 2] << 8) + resp[ALOG_RESP_SIZE - 1];
                bres = true;
            }

            return bres;
        }

        /// <summary>
        /// Send an OUT command.
        /// </summary>
        public bool Send_OUT_Command(int outNb, int outVal, int openDrain, ref string response)
        {
            byte[] cmd = new byte[6] { 0x4F, 0x00, 0x03, (byte)outNb, (byte)outVal, (byte)openDrain };
            bool bres = false;
            byte[] resp = new byte[OUT_RESP_SIZE];

            // Send OUT command, wait only ACK but no response value.
            if (SendCommand(cmd, OUT_RESP_SIZE, ref resp))
            {
                response = BitConverter.ToString(resp, 0, OUT_RESP_SIZE);
                bres = true;
            }

            return bres;
        }

        /// <summary>
        /// Send a BOOT command.
        /// </summary>
        public bool Send_BOOT_Command(int bootNb, bool bootState, ref string response)
        {
            int state = bootState ? 1 : 0; // Convert bool to int
            byte[] cmd = new byte[5] { 0x42, 0x00, 0x02, (byte)bootNb, (byte)state };
            bool bres = false;
            byte[] resp = new byte[OUT_RESP_SIZE];

            // Send BOOT command, wait only ACK but no response value.
            if (SendCommand(cmd, OUT_RESP_SIZE, ref resp))
            {
                response = BitConverter.ToString(resp, 0, OUT_RESP_SIZE);
                bres = true;
            }

            return bres;
        }

        /// <summary>
        /// Send a USB POWER command.
        /// </summary>
        public bool Send_USBPWR_Command(int usbNb, bool usbState, ref string response)
        {
            byte[] cmd = new byte[5] { 0x50, 0x00, 0x02, 0x00, 0x00 };
            bool bres = false;
            byte[] resp = new byte[OUT_RESP_SIZE];

            int state = usbState ? 1 : 0;

            // Build BOOT command
            cmd[BOOT_NB_BYTE] = (byte)usbNb;
            cmd[BOOT_STATE_BYTE] = (byte)state;

            // Send IN command, wait only ACK but no response value.
            if (SendCommand(cmd, OUT_RESP_SIZE, ref resp))
            {
                response = BitConverter.ToString(resp, 0, OUT_RESP_SIZE);
                bres = true;
            }

            return bres;
        }

        /// <summary>
        /// Get an VERSION command.
        /// </summary>
        public bool Get_VERSION_Command(ref string response, ref string value)
        {
            byte[] cmd = { 0x56, 0x00, 0x00 };
            bool bres = false;
            byte[] resp = new byte[VERSION_RESP_SIZE];
            string hexString;
            string hexChar;
            int hexValue;

            // Send VERSION command, wait only ACK but no response value.
            if (SendCommand(cmd, VERSION_RESP_SIZE, ref resp))
            {
                response = BitConverter.ToString(resp, 0, VERSION_RESP_SIZE);
                hexString = BitConverter.ToString(resp, 3, LENGTH_VERSION).Replace("-", "");

                //create the string value in ASCII
                for (int i = 0; i < hexString.Length / 2; i++)
                {
                    //Select 2 numbers
                    hexChar = hexString.Substring(i * 2, LENGTH_CHAR);
                    //Convert the numbers to hexadecimal
                    hexValue = Convert.ToInt32(hexChar, 16);
                    //Convert the hexadecimal to ASCII and add to value
                    value += Char.ConvertFromUtf32(hexValue);
                }
                bres = true;
            }

            return bres;
        }

        /// <summary>
        /// Get an BOARD_NUMBER command.
        /// </summary>
        public bool Get_BOARD_NUMBER_Command(ref string response, ref int value)
        {
            byte[] cmd = { 0x62, 0x00, 0x00 };
            bool bres = false;
            byte[] resp = new byte[BOARD_NUMBER_RESP_SIZE];

            // Send BOARD_NUMBER command, wait only ACK but no response value.
            if (SendCommand(cmd, BOARD_NUMBER_RESP_SIZE, ref resp))
            {
                response = BitConverter.ToString(resp, 0, BOARD_NUMBER_RESP_SIZE);
                value = resp[3];
                bres = true;
            }

            return bres;
        }

        #endregion

        #region Handler
        /// <summary>
        /// Event for USB arrival.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Arrival(object sender, USBEvent e)
        {
            string devicePath = e.DevicePath.ToString();

            if (devicePath.Contains(VID_PID))
            {
                if (!_arrival)
                {
                    _arrival = true;
                    _removal = false;
                    _uSBArrivalEvent(sender, e);
                }
            }
        }

        /// <summary>
        /// Event for USB Removal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Removal(object sender, USBEvent e)
        {
            string devicePath = e.DevicePath.ToString();

            if (devicePath.Contains(VID_PID))
            {
                if (!_removal)
                {
                    _removal = true;
                    _arrival = false;
                    _uSBRemovalEvent(sender, e);
                }
            }             
        } 
        #endregion
    }   
}
